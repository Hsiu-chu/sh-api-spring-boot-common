package com.sh.api.jwt;

import com.sh.api.core.Result;
import com.sh.api.core.ResultCode;
import com.sh.api.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * 类名：AuthenticationInterceptor
 * 类说明：鉴权拦截器
 * Copyright: Copyright (c) 2012-2020
 * Company: HT
 *
 * @author haoxiuzhu
 * @version 1.0
 * @date 2020/5/8
 */
public class AuthenticationInterceptor implements HandlerInterceptor {
    private final Logger logger = LoggerFactory.getLogger(AuthenticationInterceptor.class);
    @Autowired
    private TokenProvider tokenProvider;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception{
        // 如果不是映射到方法直接通过
        if(!(object instanceof HandlerMethod)){
            return true;
        }
        HandlerMethod handlerMethod=(HandlerMethod)object;
        Method method=handlerMethod.getMethod();
        //检查是否有passtoken注释，有则跳过认证
        if (method.isAnnotationPresent(PassToken.class)) {
            PassToken passToken = method.getAnnotation(PassToken.class);
            if (passToken.required()) {
                return true;
            }
        }
        //检查有没有需要用户权限的注解
        if (method.isAnnotationPresent(UserLoginToken.class)) {
            UserLoginToken userLoginToken = method.getAnnotation(UserLoginToken.class);
            if (userLoginToken.required()) {
                if(!tokenProvider.validateToken()){
                    logger.warn("token认证失败，请求接口：{}，请求IP：{}，请求参数：{}",
                            request.getRequestURI(), CommonUtil.getIpAddress(), CommonUtil.jsonToString(request.getParameterMap()));
                    Result result = new Result();
                    result.setCode(ResultCode.UNAUTHORIZED).setMessage("token失败,请重新登录");
                    CommonUtil.responseResult(response, CommonUtil.jsonToString(result));
                    return false;
                }
            }
        }
        return true;
    }

}
