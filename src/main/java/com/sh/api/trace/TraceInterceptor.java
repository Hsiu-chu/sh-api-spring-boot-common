package com.sh.api.trace;

import org.slf4j.MDC;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 类名：TraceInterceptor
 * 类说明：
 * Copyright: Copyright (c) 2012-2021
 * Company: HT
 * @author haoxiuzhu
 * @date 2021/7/7
 * @version 1.0
 */
public class TraceInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String trackId =  request.getHeader(TraceLogUtils.HEADER_PARAMETER_TRACE_ID);
        if(trackId == null){
            //存在单独使用or jwt配合aop使用2中case
            trackId =  MDC.get(TraceLogUtils.LOG_TRACE_ID);
            if(trackId == null){
                trackId = TraceLogUtils.getTraceId();
            }
        }
        MDC.put(TraceLogUtils.LOG_TRACE_ID, trackId);
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        MDC.remove(TraceLogUtils.LOG_TRACE_ID);
    }
}
