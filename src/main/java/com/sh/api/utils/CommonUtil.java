package com.sh.api.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * 类名：CommonUtil
 * 类说明：帮助工具类
 * Copyright: Copyright (c) 2012-2020
 * Company: HT
 *
 * @author haoxiuzhu
 * @version 1.0
 * @date 2020/5/8
 */
public class CommonUtil {
    private final static Logger logger = LoggerFactory.getLogger(CommonUtil.class);
    /**
     *方法:getIpAddress
     *方法说明: 获取真实ip地址
     *@author     haoxiuzhu
     *@date       2020/5/8
     *@Param      * @param request:
     *@retrun     * @return: java.lang.String
     */
    public static  String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        // 如果是多级代理，那么取第一个ip为客户端ip
        if (ip != null && ip.contains(",")) {
            ip = ip.substring(0, ip.indexOf(",")).trim();
        }

        return ip;
    }
    /**
     *方法:getIpAddress
     *方法说明:  获取真实ip地址
     *@author     haoxiuzhu
     *@date       2020/5/8
     *@Param      
     *@retrun     * @return: java.lang.String
     */
    public static  String getIpAddress(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        return getIpAddress(request);
    }
    /**
     *方法:responseResult
     *方法说明: response返回结果
     *@author     haoxiuzhu
     *@date       2020/5/8
     *@Param      * @param response: 
     * @param resultJson: 
     *@retrun     * @return: void
     */
    public static void responseResult(HttpServletResponse response, String resultJson) {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-type", "application/json;charset=UTF-8");
        response.setStatus(200);
        try {
            response.getWriter().write(resultJson);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }
    /**
     *方法:jsonObjectToString
     *方法说明: json对象转json字符串
     *@author     haoxiuzhu
     *@date       2020/6/4
     *@Param      * @param object:
     *@retrun     * @return: java.lang.String
     */
    public static String jsonToString(Object object){
        if(object != null){
            try {
                ObjectMapper mapper = new ObjectMapper();
                mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
                return mapper.writeValueAsString(object);
            }catch (Exception e){
                logger.warn(e.getMessage());
            }
        }
        return  null;
    }
    /**
     *方法:jsonStringToBean
     *方法说明: json字符串转json对象
     *@author     haoxiuzhu
     *@date       2020/6/4
     *@Param      * @param content: 
     * @param valueType: 
     *@retrun     * @return: T
     */
    public static  <T> T jsonStringToBean(String content, Class<T> valueType){
        if(content != null){
            try {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(content,valueType);
            }catch (Exception e){
                logger.warn(e.getMessage());
            }
        }
        return null;
    }
    /**
     *方法:jsonStringToJsonNode
     *方法说明: json字符串转json结点
     *@author     haoxiuzhu
     *@date       2020/6/4
     *@Param      * @param content: 
     *@retrun     * @return: com.fasterxml.jackson.databind.JsonNode
     */
    public static JsonNode jsonStringToJsonNode(String content){
        if(content != null){
            try {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readTree(content);
            }catch (Exception e){
                logger.warn(e.getMessage());
            }
        }
        return null;
    }
    /**
     *方法:createObjectNode
     *方法说明: 创建ObjectNode 用于创建Json对象图
     *@author     haoxiuzhu
     *@date       2020/6/4
     *@Param      
     *@retrun     * @return: com.fasterxml.jackson.databind.node.ObjectNode
     */
    public static ObjectNode createObjectNode(){
        ObjectMapper mapper = new ObjectMapper();
        return mapper.createObjectNode();
    }
    /**
     *方法:uuid
     *方法说明: 生成uuid
     *@author     haoxiuzhu
     *@date       2020/6/9
     *@Param      
     *@retrun     * @return: java.lang.String
     */
    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
