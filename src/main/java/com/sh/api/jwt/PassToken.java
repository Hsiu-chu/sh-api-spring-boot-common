package com.sh.api.jwt;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 类名：PassToken
 * 类说明：跳过验证注解
 * Copyright: Copyright (c) 2012-2020
 * Company: HT
 *
 * @author haoxiuzhu
 * @version 1.0
 * @date 2020/5/8
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PassToken {
    boolean required() default true;
}
