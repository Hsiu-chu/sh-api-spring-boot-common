package com.sh.api.exception;

/**
  * 类名：ValidateParameterException.java
  * 类说明： 
  * Copyright: Copyright (c) 2012-2021
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2021年6月9日
  * @version    1.0
*/
public class ValidateParameterException extends RuntimeException{
    private int ret;
    private String message;

    public ValidateParameterException() {
    }

    public ValidateParameterException(int ret, String message) {
        super(message);
        this.message = message;
        this.ret = ret;
    }

    public ValidateParameterException(String message) {
        super(message);
        this.ret = 409;
        this.message = message;
    }

    public int getRet() {
        return ret;
    }

    public void setRet(int ret) {
        this.ret = ret;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
