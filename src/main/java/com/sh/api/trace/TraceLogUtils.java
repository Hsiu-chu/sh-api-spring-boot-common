package com.sh.api.trace;

import java.util.UUID;

/**
 * 类名：TraceLogUtils
 * 类说明：
 * Copyright: Copyright (c) 2012-2021
 * Company: HT
 * @author haoxiuzhu
 * @date 2021/7/7
 * @version 1.0
 */
public class TraceLogUtils {
    /**日志跟踪traceid*/
    public static final String LOG_TRACE_ID = "traceid";
    /**请求头日志跟踪trace-id*/
    public static final String HEADER_PARAMETER_TRACE_ID = "trace-id";

    /**
     *方法:getTraceId
     *方法说明: 获取traceId
     *@author     haoxiuzhu
     *@date       2021/7/7
     *@Param
     *@retrun     * @return: java.lang.String
     */
    public static String getTraceId() {
        UUID uuid = UUID.randomUUID();
        String trackId = uuid.toString();
        trackId = trackId.toUpperCase();
        return trackId.replaceAll("-", "");
    }
}
