package com.sh.api.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
  * 类名：TokenProvider.java
  * 类说明： token提供者
  * Copyright: Copyright (c) 2012-2020
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2020年05月08日
  * @version    1.0
 */
@Component
@Configuration
public class TokenProvider {
    private final Logger LOGGER = LoggerFactory.getLogger(TokenProvider.class);
    /**私钥*/
    @Value("${jwt.secretKey:123456}")
    private String secretKey;
    /**token失效的毫秒数*/
    @Value("${jwt.tokenValidityInMilliseconds:120000}")
    private long tokenValidityInMilliseconds;
    /**记住我后token失效的毫秒数*/
    @Value("${jwt.tokenValidityInMillisecondsForRememberMe:3600000}")
    private long tokenValidityInMillisecondsForRememberMe;

    /**header参数key:Authorization*/
    public static final String NAME_HEADER_PARAM_AUTHORIZATION = "Authorization";
    /**Authorization值开始字符串(Bearer )*/
    public static final String STR_STARTS_WITH_AUTHORIZATION_BEARER = "Bearer ";

    /***
      * 方法：createToken 
      * 方法说明：    创建token
      * @param subject 用户信息的json字符串
      * @param rememberMe 是否记住我 true:记住我 flase:不记住
      * @return
      * @author     ht-haoxiuzhu
      * @date       2020年05月08日
     */
    public String createToken(String subject,Boolean rememberMe) {
        long now = (new Date()).getTime();
        Date validity;
        LOGGER.debug("rememberMe:{},now:{}ms,tokenValidityInMilliseconds:{}ms,tokenValidityInMillisecondsForRememberMe:{}ms",rememberMe,now,tokenValidityInMilliseconds,tokenValidityInMillisecondsForRememberMe);
        if (rememberMe) {
            validity = new Date(now+ this.tokenValidityInMillisecondsForRememberMe);
        } else {
            validity = new Date(now + this.tokenValidityInMilliseconds);
        }
        LOGGER.debug("validity:{}ms,secretKey:{}",validity,secretKey);
        String jwToken = Jwts.builder().setSubject(subject)
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setExpiration(validity).compact();
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getResponse();
        response.addHeader(NAME_HEADER_PARAM_AUTHORIZATION,STR_STARTS_WITH_AUTHORIZATION_BEARER+jwToken);
        return jwToken;
    }
    /**
      * 方法：validateToken 
      * 方法说明：    校验token的有效性
      * @param authToken
      * @return true:token有效 false:token无效
      * @author     ht-haoxiuzhu
      * @date       2020年05月08日
     */
    public boolean validateToken(String authToken) {
        LOGGER.info("authToken:{}",authToken);
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            LOGGER.info("Invalid JWT signature.");
            LOGGER.trace("Invalid JWT signature trace: {}", e);
        } catch (MalformedJwtException e) {
            LOGGER.info("Invalid JWT token.");
            LOGGER.trace("Invalid JWT token trace: {}", e);
        } catch (ExpiredJwtException e) {
            LOGGER.info("Expired JWT token.");
            LOGGER.trace("Expired JWT token trace: {}", e);
        } catch (UnsupportedJwtException e) {
            LOGGER.info("Unsupported JWT token.");
            LOGGER.trace("Unsupported JWT token trace: {}", e);
        } catch (IllegalArgumentException e) {
            LOGGER.info("JWT token compact of handler are invalid.");
            LOGGER.trace("JWT token compact of handler are invalid trace: {}", e);
        }
        return false;
    }
    /**
     * 方法：validateToken
     * 方法说明：    校验token的有效性
     * @return true:token有效 false:token无效
     * @author     ht-haoxiuzhu
     * @date       2020年05月08日
     */
    public boolean validateToken(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String jwt =  request.getHeader(NAME_HEADER_PARAM_AUTHORIZATION);
        if (StringUtils.hasText(jwt) && jwt.startsWith(STR_STARTS_WITH_AUTHORIZATION_BEARER)) {
            jwt = jwt.substring(STR_STARTS_WITH_AUTHORIZATION_BEARER.length(), jwt.length());
            return validateToken(jwt);
        }
        return  false;
    }
    /***
      * 方法：getSubject 
      * 方法说明：    获取用户信息的json字符串
      * @return
      * @author     ht-haoxiuzhu
      * @date       2020年05月08日
     */
    public String getSubject(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String jwt =  request.getHeader(NAME_HEADER_PARAM_AUTHORIZATION);
        if (StringUtils.hasText(jwt) && jwt.startsWith(STR_STARTS_WITH_AUTHORIZATION_BEARER)) {
            jwt = jwt.substring(STR_STARTS_WITH_AUTHORIZATION_BEARER.length(), jwt.length());
        }
        if (!validateToken(jwt)){
            return null;
        }
        Claims claims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(jwt)
                .getBody();
        return claims.getSubject();
    }
}
